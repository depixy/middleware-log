import type { Middleware } from "koa";

import {
  createLogger,
  format,
  Logger,
  LoggerOptions,
  transports
} from "winston";

declare module "koa" {
  interface ExtendableContext {
    logger: Logger;
  }
}
const isProduction = process.env["NODE_ENV"] === "production";

const defaultOption = {
  level: isProduction ? "info" : "debug",
  transports: [new transports.Console()],
  format: format.combine(
    format.timestamp(),
    format.printf(info => {
      const { timestamp, level, message } = info;
      return `${timestamp} [${level}] ${message}`;
    })
  )
};

const middleware = async (
  option: LoggerOptions = defaultOption
): Promise<Middleware> => {
  const logger = createLogger(option);
  return async (ctx, next) => {
    ctx.logger = logger;
    await next();
  };
};

export default middleware;
