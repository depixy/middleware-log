## [1.0.1](https://gitlab.com/depixy/middleware-log/compare/v1.0.0...v1.0.1) (2020-07-23)


### Bug Fixes

* initial logger on create ([3a165bf](https://gitlab.com/depixy/middleware-log/commit/3a165bf1588b7b863d17c81de451367d50098bec))

# 1.0.0 (2020-07-20)


### Features

* initial commit ([eb6aa5d](https://gitlab.com/depixy/middleware-log/commit/eb6aa5d0f49d8756334fe0ab28e776faf8d48613))
