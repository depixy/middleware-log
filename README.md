# @depixy/middleware-log

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![Coverage][coverage_badge]][pipelines] [![NPM][npm_badge]][npm] [![semantic-release][semantic_release_badge]][semantic_release]

Logger middleware for Depixy.

## Installation

```
npm i @depixy/middleware-log
```

[license]: https://gitlab.com/depixy/middleware-log/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[pipelines]: https://gitlab.com/depixy/middleware-log/pipelines
[pipelines_badge]: https://gitlab.com/depixy/middleware-log/badges/master/pipeline.svg
[coverage_badge]: https://gitlab.com/depixy/middleware-log/badges/master/coverage.svg
[npm]: https://www.npmjs.com/package/@depixy/middleware-log
[npm_badge]: https://img.shields.io/npm/v/@depixy/middleware-log/latest.svg
[semantic_release]: https://github.com/semantic-release/semantic-release
[semantic_release_badge]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
